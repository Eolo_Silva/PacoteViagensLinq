﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class Genericos
    {
        Pacote pacote = new Pacote();
        public List<Pacote> ListaDePacotes;

        public override string ToString()
        {
            return string.Format("Id Pack:{0} - Pack:{1} - Preço Pack:{2}", pacote.Id, pacote.Descricao, pacote.Preco);
        }
        public override int GetHashCode()
        {
            return pacote.Id;
        }
        public int GeraIdPacote()
        {
            return ListaDePacotes[ListaDePacotes.Count - 1].Id + 1;
        }
        public void GravarFile()
        {
            if (localizarFicheiro != null)
            {
                StreamWriter sw = new StreamWriter(localizarFicheiro);
                try
                {
                    if (!File.Exists(localizarFicheiro))
                    {
                        sw = File.CreateText(localizarFicheiro);
                    }

                    foreach (var pacote in ListaDePacotes)
                    {
                        string linha = string.Format("{0};{1};{2}", pacote.Id, pacote.Descricao, pacote.Preco);
                        sw.WriteLine(linha);
                    }

                    sw.Close();
                }
                catch (Exception ev)
                {
                    MessageBox.Show(ev.Message);
                }
            }
            else
            {
                MessageBox.Show("Tem que gravar como.. primeiro");
            }
        }
        public void LerFile()
        {
            StreamReader myStream;
            OpenFileDialog OpenFileDialog1 = new OpenFileDialog();

            OpenFileDialog1.Filter = "Text file|*.txt";
            OpenFileDialog1.RestoreDirectory = true;

            if (OpenFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = new StreamReader(OpenFileDialog1.OpenFile())) != null)
                    {
                        using (myStream)
                        {
                            ListaDePacotes.Clear();
                            string linha = "";

                            while ((linha = myStream.ReadLine()) != null)
                            {
                                string[] campos = new string[3];
                                campos = linha.Split(';');

                                var pacote = new Pacote
                                {
                                    Id = Convert.ToInt32(campos[0]),
                                    Descricao = campos[1],
                                    Preco = Convert.ToDouble(campos[2])
                                };

                                ListaDePacotes.Add(pacote);
                            }

                            myStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: Não foi possível ler o arquivo do disco. Erro original: " + ex.Message);
                }
            }
        }

        public string localizarFicheiro = null;

        public void SalvarComoFile()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file|*.txt";
            saveFileDialog1.Title = "Salvar Como Ficheiro de Texto";
            saveFileDialog1.ShowDialog();

            // Se o nome do arquivo não for uma seqüência vazia, abra-o para salvar.
            if (saveFileDialog1.FileName != "")
            {
                // Salva a imagem através de um FileStream criado pelo método OpenFile.
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();

                StreamWriter sw = new StreamWriter(fs);//se nao existir, grava

                //verficar o conteudo da lista de pacotes de viagem
                foreach (var pacote in ListaDePacotes)
                {
                    string linha = string.Format("{0};{1};{2}", pacote.Id, pacote.Descricao, pacote.Preco);
                    sw.WriteLine(linha);

                }
                sw.Close();
                fs.Close();

                localizarFicheiro = saveFileDialog1.FileName;
            }
        }

        public void AcercaDe()
        {
            MessageBox.Show("Formando:\nEolo Guerra Silva\nCET30 n.6\nPacote de Viagens Versão 1.0.0\n06-10-2017");
        }
    }
}
