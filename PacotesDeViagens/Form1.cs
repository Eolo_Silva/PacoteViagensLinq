﻿namespace PacotesDeViagens
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.IO;

    public partial class Form1 : Form
    {
        FormApagarPacote fap = new FormApagarPacote();
        FormAtualizarPacote fup = new FormAtualizarPacote();
        FormCriarPacote fcp = new FormCriarPacote();
        FormLerPacote flp = new FormLerPacote();
        public Form1()
        {
            InitializeComponent();
            ControlBox = false;
        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            fcp.Show();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            fup.Show();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            fap.Show();
        }

        private void ButtonLer_Click(object sender, EventArgs e)
        {
            flp.Show();
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
