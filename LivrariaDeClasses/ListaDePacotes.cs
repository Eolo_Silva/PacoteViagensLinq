﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class ListaDePacotes
    {
        public static List<Pacote> LoadPacotes()
        {
            List<Pacote> output = new List<Pacote>();

            output.Add(new Pacote { Id = 1, Descricao = "Nova York", Preco = 987.3 });
            output.Add(new Pacote { Id = 2, Descricao = "Paris", Preco = 232.4 });
            output.Add(new Pacote { Id = 3, Descricao = "Amazonas", Preco = 1800.5 });
            output.Add(new Pacote { Id = 4, Descricao = "Douro", Preco = 187.2 });
            output.Add(new Pacote { Id = 5, Descricao = "Piramides", Preco = 567.8 });
            output.Add(new Pacote { Id = 6, Descricao = "Algarve", Preco = 220 });

            return output;
        }
    }
}
