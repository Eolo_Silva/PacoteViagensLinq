﻿namespace PacotesDeViagens
{
    partial class FormLerPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridViewLer = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preço = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonSairLer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewLer)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewLer
            // 
            this.DataGridViewLer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewLer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Descrição,
            this.Preço});
            this.DataGridViewLer.Location = new System.Drawing.Point(3, 2);
            this.DataGridViewLer.Name = "DataGridViewLer";
            this.DataGridViewLer.Size = new System.Drawing.Size(343, 354);
            this.DataGridViewLer.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // Descrição
            // 
            this.Descrição.HeaderText = "Descrição";
            this.Descrição.Name = "Descrição";
            this.Descrição.ReadOnly = true;
            // 
            // Preço
            // 
            this.Preço.HeaderText = "Preço";
            this.Preço.Name = "Preço";
            this.Preço.ReadOnly = true;
            // 
            // ButtonSairLer
            // 
            this.ButtonSairLer.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSairLer.BackgroundImage = global::PacotesDeViagens.Properties.Resources.exit;
            this.ButtonSairLer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonSairLer.Location = new System.Drawing.Point(304, 362);
            this.ButtonSairLer.Name = "ButtonSairLer";
            this.ButtonSairLer.Size = new System.Drawing.Size(41, 33);
            this.ButtonSairLer.TabIndex = 1;
            this.ButtonSairLer.UseVisualStyleBackColor = false;
            this.ButtonSairLer.Click += new System.EventHandler(this.ButtonSairLer_Click);
            // 
            // FormLerPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 403);
            this.Controls.Add(this.ButtonSairLer);
            this.Controls.Add(this.DataGridViewLer);
            this.Name = "FormLerPacote";
            this.Text = "FormLer";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewLer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewLer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preço;
        private System.Windows.Forms.Button ButtonSairLer;
    }
}