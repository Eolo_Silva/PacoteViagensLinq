﻿namespace PacotesDeViagens
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.ButtonLer = new System.Windows.Forms.Button();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = global::PacotesDeViagens.Properties.Resources.exit;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonSair.Location = new System.Drawing.Point(577, 4);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(39, 38);
            this.ButtonSair.TabIndex = 0;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCriar.BackgroundImage = global::PacotesDeViagens.Properties.Resources.create;
            this.ButtonCriar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCriar.Location = new System.Drawing.Point(183, 292);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 68);
            this.ButtonCriar.TabIndex = 1;
            this.ButtonCriar.UseVisualStyleBackColor = false;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // ButtonLer
            // 
            this.ButtonLer.BackColor = System.Drawing.Color.Transparent;
            this.ButtonLer.BackgroundImage = global::PacotesDeViagens.Properties.Resources.read;
            this.ButtonLer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonLer.Location = new System.Drawing.Point(287, 292);
            this.ButtonLer.Name = "ButtonLer";
            this.ButtonLer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ButtonLer.Size = new System.Drawing.Size(75, 68);
            this.ButtonLer.TabIndex = 2;
            this.ButtonLer.UseVisualStyleBackColor = false;
            this.ButtonLer.Click += new System.EventHandler(this.ButtonLer_Click);
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.BackColor = System.Drawing.Color.Transparent;
            this.ButtonUpdate.BackgroundImage = global::PacotesDeViagens.Properties.Resources.update;
            this.ButtonUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonUpdate.Location = new System.Drawing.Point(385, 292);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(75, 68);
            this.ButtonUpdate.TabIndex = 3;
            this.ButtonUpdate.UseVisualStyleBackColor = false;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.BackColor = System.Drawing.Color.Transparent;
            this.ButtonDelete.BackgroundImage = global::PacotesDeViagens.Properties.Resources.delete;
            this.ButtonDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonDelete.Location = new System.Drawing.Point(492, 292);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(75, 68);
            this.ButtonDelete.TabIndex = 4;
            this.ButtonDelete.UseVisualStyleBackColor = false;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::PacotesDeViagens.Properties.Resources.backGround;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(621, 372);
            this.Controls.Add(this.ButtonDelete);
            this.Controls.Add(this.ButtonUpdate);
            this.Controls.Add(this.ButtonLer);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.ButtonSair);
            this.Name = "Form1";
            this.Text = "Pacote de Viagens Linq";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonLer;
        private System.Windows.Forms.Button ButtonUpdate;
        private System.Windows.Forms.Button ButtonDelete;
    }
}

